# Apache Flink Tutorial

A demonstration how to achieve efficient, distributed, general-purpose data processing using Apache Flink.

Please feel free to have a look at [my blog] for the full tutorial.

----

**2015 Micha Kops / hasCode.com**

   [my blog]:http://www.hascode.com/
